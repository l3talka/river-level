import Vue from 'vue';
import Router from 'vue-router';
import RiverLevel from './components/RiverLevel';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'RiverLevel',
      component: RiverLevel,
    },
  ],
});
